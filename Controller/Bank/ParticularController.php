<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\GenericBundle\Controller\Bank;

use App\Entity\Application\GenericMaster;
use App\Repository\Application\GenericMasterRepository;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\GenericBundle\Entity\Particular;
use Terminalbd\GenericBundle\Entity\ParticularType;
use Terminalbd\GenericBundle\Form\Bank\ParticularFormType;
use Terminalbd\GenericBundle\Repository\ParticularRepository;


/**
 * @Route("/gmb/bank/particular")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ParticularController extends AbstractController
{


    /**
     * @Route("/", methods={"GET", "POST"}, name="gmb_bank_particular")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_OFFICER')")
     */
    public function index(Request $request, TranslatorInterface $translator, GenericMasterRepository $masterRepository): Response
    {

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $masterRepository->config($terminal);
        $entity = new Particular();
        $data = $request->request->all();
        $form = $this->createForm(ParticularFormType::class , $entity)->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setConfig($config);
            $em->persist($entity);
            $em->flush();
            $message = $translator->trans('data.created_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('gmb_bank_particular');
        }
        return $this->render('@TerminalbdGeneric/bank/particular/index.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }
    
    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="gmb_bank_particular_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_CORE') or is_granted('ROLE_DOMAIN')")
     */
    public function show($id): Response
    {
        $repository = $this->getDoctrine()->getRepository(Particular::class);
        $entity = $repository->find($id);
        $html = $this->renderView(
            '@TerminalbdGeneric/bank/particular/show.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }


     /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="gmb_bank_particular_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_CORE') or is_granted('ROLE_DOMAIN')")
     */
    public function status($id): Response
    {
        $repository = $this->getDoctrine()->getRepository(Particular::class);
        $entity = $repository->find($id);
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setStatus(false);
        }else{
            $entity->setStatus(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="gmb_bank_particular_edit")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN')")
     */
    public function edit(Request $request, $id,TranslatorInterface $translator, GenericMasterRepository $genericMasterRepository, ParticularRepository $repository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        /* @var $config  GenericMaster */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $genericMasterRepository->config($terminal);

        /* @var $entity Particular */
        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));

        $form = $this->createForm(ParticularFormType::class, $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('gmb_bank_particular');
        }
        return $this->render('@TerminalbdGeneric/bank/particular/index.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="gmb_bank_particular_delete")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN')")
     */

    public function delete($id, SettingRepository $repository): Response
    {

        /* @var $config  GenericMaster */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal);
        $repository = $this->getDoctrine()->getRepository(Particular::class);
        /* @var $entity Particular */
        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));

        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particular entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            $response = 'valid';

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/meta-delete", methods={"GET"}, name="gmb_bank_particular_meta_delete")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN')")
     */
    public function metaDelete($id, ItemKeyValueRepository $keyValueRepository): Response
    {
        $entity = $keyValueRepository->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        return new Response('Success');
    }

    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="gmb_bank_particular_data_table" , options={"expose"=true})
     * @Security("is_granted('ROLE_CORE') or is_granted('ROLE_USER') or is_granted('ROLE_DOAMIN')")
     */

    public function dataTable(Request $request, GenericMasterRepository $genericMasterRepository , ParticularRepository $repository)
    {

        /* @var $config  GenericMaster */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $genericMasterRepository->config($terminal);
        $entities = $repository->findBy(array('config'=>$config));
        $iTotalRecords = $repository->count(array('config'=> $config));

        $i = 1;
        $records = array();
        $records["data"] = array();

        /* @var $post Particular */

        foreach ($entities as $post):

            $active = empty($post->isStatus()) ? '' : "checked";
            $status ="<input class='status' data-action='{$this->generateUrl('gmb_bank_particular_status',array('id'=>$post->getId()))}' type='checkbox' {$active} data-toggle='toggle' data-size='xs' data-style='slow' data-offstyle='warning' data-onstyle='info' data-on='Enabled'  data-off='Disabled'>";

            $records["data"][] = array(
                $id                 = $i,
                $type            = $post->getParticularType()->getName(),
                $name               = $post->getName(),
                $slug               = $post->getSlug(),
                $code               = $post->getCode(),
                $status             = $status,
                $action             ="<a class='btn btn-mini yellow-bg white-font' href='{$this->generateUrl('gmb_bank_particular_edit',array('id'=>$post->getId()))}' id='{$post->getId()}' ><i class='feather icon-edit'></i> Edit</a>
<a class='btn btn-mini blue-bg white-font entity-show' data-action='{$this->generateUrl('gmb_bank_particular_show',array('id'=>$post->getId()))}' href='javascript:' data-title='{$post->getName()}' id='postView-{$post->getId()}' ><i class='feather icon-eye'></i> Show</a>
<a class='btn  btn-transparent btn-mini red-font remove' data-id='{$post->getId()}' href='javascript:' data-action='{$this->generateUrl('gmb_bank_particular_delete',array('id'=>$post->getId()))}' id='{$post->getId()}' ><i class='feather icon-trash-2'></i></a>
");
            $i++;
        endforeach;
        return new JsonResponse($records);
    }


}

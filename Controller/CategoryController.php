<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\GenericBundle\Controller;

use App\Entity\Application\GenericMaster;
use App\Repository\Application\GenericMasterRepository;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\GenericBundle\Entity\ItemUnit;
use Terminalbd\GenericBundle\Entity\Particular;
use Terminalbd\GenericBundle\Entity\ParticularType;
use Terminalbd\GenericBundle\Form\CategoryFormType;
use Terminalbd\GenericBundle\Form\GenericParticularFormType;
use Terminalbd\GenericBundle\Form\ParticularFormType;
use Terminalbd\GenericBundle\Repository\CategoryMetaRepository;
use Terminalbd\GenericBundle\Repository\CategoryRepository;
use Terminalbd\GenericBundle\Repository\ItemKeyValueRepository;
use Terminalbd\GenericBundle\Repository\ParticularRepository;


/**
 * @Route("/gmb/category")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class CategoryController extends AbstractController
{


    /**
     * @Route("/", methods={"GET", "POST"}, name="gmb_category")
     * @Security("is_granted('ROLE_BUDGET_ADMIN') or is_granted('ROLE_DOMAIN')")
     */
    public function index(Request $request, TranslatorInterface $translator, GenericMasterRepository $masterRepository): Response
    {

        $entity = new Category();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $masterRepository->config($terminal);
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $data = $request->request->all();
        $form = $this->createForm(CategoryFormType::class , $entity,array('categoryRepo' => $categoryRepo,'config' => $config))->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setConfig($config);
            $em->persist($entity);
            $em->flush();
            $message = $translator->trans('data.created_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('gmb_category');
        }
        return $this->render('@TerminalbdGeneric/category/index.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }
    
     /**
     * @Route("/new", methods={"GET", "POST"}, name="gmb_category_new")
     * @Security("is_granted('ROLE_BUDGET_ADMIN') or is_granted('ROLE_DOMAIN')")
     */
    public function new(Request $request, TranslatorInterface $translator, GenericMasterRepository $masterRepository ,  CategoryMetaRepository $metaRepository): Response
    {

        $entity = new Category();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $masterRepository->config($terminal);
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $data = $request->request->all();
        $form = $this->createForm(CategoryFormType::class , $entity,array('categoryRepo' => $categoryRepo,'config' => $config))->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setConfig($config);
            $em->persist($entity);
            $em->flush();
            $message = $translator->trans('data.created_successfully');
            $this->addFlash('success', $message);
          //  $metaRepository->categoryMeta($entity,$data);
            return $this->redirectToRoute('gmb_category');
        }
        return $this->render('@TerminalbdGeneric/category/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="gmb_category_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_BUDGET_ADMIN') or is_granted('ROLE_DOMAIN')")
     */
    public function show($id): Response
    {
        $repository = $this->getDoctrine()->getRepository(Particular::class);
        $entity = $repository->find($id);
        $html = $this->renderView(
            '@TerminalbdGeneric/particular/show.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }


     /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="gmb_category_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_BUDGET_ADMIN') or is_granted('ROLE_DOMAIN')")
     */
    public function status($id): Response
    {
        $repository = $this->getDoctrine()->getRepository(Category::class);
        $entity = $repository->find($id);
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setStatus(false);
        }else{
            $entity->setStatus(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="gmb_category_edit")
     * @Security("is_granted('ROLE_BUDGET_ADMIN') or is_granted('ROLE_DOMAIN')")
     */
    public function edit(Request $request, $id,TranslatorInterface $translator, GenericMasterRepository $masterRepository, CategoryMetaRepository $metaRepository): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $masterRepository->config($terminal);
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        /* @var $entity Category */
        $entity = $categoryRepo->findOneBy(array('config' => $config,'id' => $id));
        $form = $this->createForm(CategoryFormType::class , $entity,array('categoryRepo' => $categoryRepo,'config' => $config))->add('SaveAndCreate', SubmitType::class);
        $form->remove('config');
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();
            $this->getDoctrine()->getManager()->flush();
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
          //  $metaRepository->categoryMeta($entity,$data);
            return $this->redirectToRoute('gmb_category_edit',array('id'=> $entity->getId()));
        }
        return $this->render('@TerminalbdGeneric/category/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="gmb_category_delete")
     * @Security("is_granted('ROLE_BUDGET_ADMIN') or is_granted('ROLE_DOMAIN')")
     */

    public function delete($id): Response
    {

        /* @var $config  GenericMaster */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal);
        $repository = $this->getDoctrine()->getRepository(Category::class);
        /* @var $entity Category */
        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));
        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }
        $entity->setIsDelete(1);
        $em->flush();
        $response = 'valid';
        return new Response($response);
    }


    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="gmb_category_data_table" , options={"expose"=true})
     * @Security("is_granted('ROLE_BUDGET_ADMIN') or is_granted('ROLE_USER') or is_granted('ROLE_DOAMIN')")
     */

    public function dataTable(Request $request, GenericMasterRepository $genericMasterRepository)
    {

        /* @var $config  GenericMaster */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $genericMasterRepository->config($terminal);
        $entities = $this->getDoctrine()->getRepository(Category::class)->findBy(array('config' => $config,'isDelete'=> 0));
        $iTotalRecords = $this->getDoctrine()->getRepository(Category::class)->count(array('config'=> $config));

        $i = 1;
        $records = array();
        $records["data"] = array();

        /* @var $post Category */

        foreach ($entities as $post):

            $active = empty($post->isStatus()) ? '' : "checked";
            $parent = empty($post->getParent()) ? '' : $post->getParent()->getName();
            $generalLedger = empty($post->getGeneralLedger()) ? '' : $post->getGeneralLedger()->getName();
            $department = ($post->getGeneralLedger() and $post->getGeneralLedger()->getDepartment()) ? $post->getGeneralLedger()->getDepartment()->getName() : '';
            $status ="<input class='status' data-action='{$this->generateUrl('gmb_category_status',array('id'=>$post->getId()))}' type='checkbox' {$active} data-toggle='toggle' data-size='xs' data-style='slow' data-offstyle='warning' data-onstyle='info' data-on='Enabled'  data-off='Disabled'>";
            $gl = '';
            if($post->getGeneralLedger()){
                $gl = $post->getGeneralLedger()->getNameWithCode();
            }
            $records["data"][] = array(
                $id                 = $i,
                $type               = $post->getItemMode(),
                $gl                 = $generalLedger,
                $department         = $department,
                $name               = $post->getName(),
                $gl                 = $parent,
                $status             = $status,
                $action             ="<a class='btn btn-mini yellow-bg white-font' href='{$this->generateUrl('gmb_category_edit',array('id'=>$post->getId()))}' id='{$post->getId()}' ><i class='feather icon-edit'></i> Edit</a>
<a class='btn  btn-transparent btn-mini red-font remove' data-id='{$post->getId()}' href='javascript:' data-action='{$this->generateUrl('gmb_category_delete',array('id'=>$post->getId()))}' id='{$post->getId()}' ><i class='feather icon-trash-2'></i></a>
");
            $i++;
        endforeach;
        return new JsonResponse($records);
    }


    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/meta-delete", methods={"GET"}, name="gmb_category_meta_delete")
     * @Security("is_granted('ROLE_BUDGET_ADMIN') or is_granted('ROLE_DOMAIN')")
     */
    public function metaDelete($id, CategoryMetaRepository $metaRepository): Response
    {
        $entity = $metaRepository->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        return new Response('Success');
    }


}

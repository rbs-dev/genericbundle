<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\GenericBundle\Controller;

use App\Entity\Application\GenericMaster;
use App\Repository\Application\GenericMasterRepository;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\GenericBundle\Entity\ItemSize;
use Terminalbd\GenericBundle\Entity\Particular;
use Terminalbd\GenericBundle\Entity\ParticularType;
use Terminalbd\GenericBundle\Form\GenericParticularFormType;
use Terminalbd\GenericBundle\Form\ParticularFormType;
use Terminalbd\GenericBundle\Form\SizeFormType;
use Terminalbd\GenericBundle\Repository\ItemKeyValueRepository;
use Terminalbd\GenericBundle\Repository\ItemSizeRepository;
use Terminalbd\GenericBundle\Repository\ParticularRepository;


/**
 * @Route("/gmb/item-size")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class SizeController extends AbstractController
{


    /**
     * @Route("/", methods={"GET", "POST"}, name="gmb_size")
     * @Security("is_granted('ROLE_INVENTORY_MANAGER') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_OFFICER')")
     */
    public function index(Request $request, TranslatorInterface $translator, GenericMasterRepository $masterRepository ,  ItemKeyValueRepository $keyValueRepository): Response
    {

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $masterRepository->config($terminal);
        $entity = new ItemSize();
        $data = $request->request->all();
        $form = $this->createForm(SizeFormType::class , $entity,['config'=>$config])->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setConfig($config);
            $em->persist($entity);
            $em->flush();
            $message = $translator->trans('data.created_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('gmb_size');
        }
        return $this->render('@TerminalbdGeneric/size/index.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

     /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="gmb_size_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_INVENTORY_MANAGER') or is_granted('ROLE_DOMAIN')")
     */
    public function status($id): Response
    {
        $repository = $this->getDoctrine()->getRepository(ItemSize::class);
        $entity = $repository->find($id);
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setStatus(false);
        }else{
            $entity->setStatus(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="gmb_size_edit")
     * @Security("is_granted('ROLE_INVENTORY_MANAGER') or is_granted('ROLE_DOMAIN')")
     */
    public function edit(Request $request, $id,TranslatorInterface $translator, GenericMasterRepository $genericMasterRepository, ItemSizeRepository $repository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        /* @var $config  GenericMaster */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $genericMasterRepository->config($terminal);

        /* @var $entity Particular */
        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));

        $form = $this->createForm(SizeFormType::class, $entity,['config' => $config])
            ->add('SaveAndCreate', SubmitType::class);
        $form->remove('config');
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('gmb_size');
        }
        return $this->render('@TerminalbdGeneric/size/index.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="gmb_size_delete")
     * @Security("is_granted('ROLE_INVENTORY_MANAGER') or is_granted('ROLE_DOMAIN')")
     */

    public function delete($id, ItemSizeRepository $repository): Response
    {

        /* @var $config  GenericMaster */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal);
        $repository = $this->getDoctrine()->getRepository(ItemSize::class);
        /* @var $entity Particular */
        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));

        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particular entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            $response = 'valid';

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }


    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="gmb_size_data_table" , options={"expose"=true})
     * @Security("is_granted('ROLE_INVENTORY_MANAGER') or is_granted('ROLE_USER') or is_granted('ROLE_DOAMIN')")
     */

    public function dataTable(Request $request, GenericMasterRepository $genericMasterRepository , ItemSizeRepository $repository)
    {

        /* @var $config  GenericMaster */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $genericMasterRepository->config($terminal);
        $entities = $repository->findBy(array('config'=>$config));
       // $iTotalRecords = $repository->count(array('config'=> $config));

        $i = 1;
        $records = array();
        $records["data"] = array();

        /* @var $post Particular */

        foreach ($entities as $post):

            $active = empty($post->isStatus()) ? '' : "checked";
            $status ="<input class='itemStatus' data-action='{$this->generateUrl('gmb_size_status',array('id'=>$post->getId()))}' type='checkbox' {$active} >";

            $records["data"][] = array(
                $id                 = $i,
                $name               = $post->getName(),
                $status             = $status,
                $action             ="<a class='btn btn-mini yellow-bg white-font' href='{$this->generateUrl('gmb_size_edit',array('id'=>$post->getId()))}' id='{$post->getId()}' ><i class='feather icon-edit'></i> Edit</a>
<a class='btn  btn-transparent btn-mini red-font remove' data-id='{$post->getId()}' href='javascript:' data-action='{$this->generateUrl('gmb_size_delete',array('id'=>$post->getId()))}' id='{$post->getId()}' ><i class='feather icon-trash-2'></i></a>
");
            $i++;
        endforeach;
        return new JsonResponse($records);
    }


}

<?php

namespace Terminalbd\GenericBundle\Entity;

use App\Entity\Application\GenericMaster;
use App\Entity\Application\Inventory;
use Terminalbd\BudgetBundle\Entity\Head;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Terminalbd\InventoryBundle\Entity\StockBook;


/**
 * TallyCategory
 *
 * @Gedmo\Tree(type="materializedPath")
 * @ORM\Table(name="gmb_category")
 * @UniqueEntity(fields={"name","config"}, message="This name must be unique")
 * @ORM\Entity(repositoryClass="Terminalbd\GenericBundle\Repository\CategoryRepository")
 */


class Category
{
    /**
     * @var integer
     *
     * @Gedmo\TreePathSource
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\GenericMaster")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private $config;

    /**
     * @var Head
     * @ORM\ManyToOne(targetEntity="Terminalbd\BudgetBundle\Entity\Head")
     * @ORM\OrderBy({"name" = "ASC"})
     **/
    private $generalLedger;

    /**
     * @var string
     * @ORM\Column(type="string", length=50,nullable=true)
     */
    private $glCode;

    /**
     * @var ItemKeyValue
     * @ORM\OneToMany(targetEntity="Terminalbd\GenericBundle\Entity\CategoryMeta" , mappedBy="category")
     */
    private $categoryMetas;

    /**
     * @var StockBook
     * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\StockBook" , mappedBy="category")
     */
    private $stockBooks;

    /**
     * @var Item
     * @ORM\OneToMany(targetEntity="Terminalbd\GenericBundle\Entity\Item" , mappedBy="category")
     */
    private $items;

    
    /**
     * @ORM\ManyToMany(targetEntity="Terminalbd\GenericBundle\Entity\ItemSize")
     * @ORM\OrderBy({"name" = "ASC"})
     **/
    private $size;

    /**
     * @ORM\ManyToMany(targetEntity="Terminalbd\GenericBundle\Entity\ItemColor")
     * @ORM\OrderBy({"name" = "ASC"})
     **/
    private $color;

    /**
     * @ORM\ManyToMany(targetEntity="Terminalbd\GenericBundle\Entity\ItemBrand")
     * @ORM\OrderBy({"name" = "ASC"})
     **/
    private $brand;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=30,nullable=true)
     */
    private $itemMode;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $createdBy;


    /**
     * @Gedmo\Slug(handlers={
     *      @Gedmo\SlugHandler(class="Gedmo\Sluggable\Handler\TreeSlugHandler", options={
     *          @Gedmo\SlugHandlerOption(name="parentRelationField", value="parent"),
     *          @Gedmo\SlugHandlerOption(name="separator", value="-")
     *      })
     * }, fields={"name"})
     * @Doctrine\ORM\Mapping\Column(length=255, unique=true)
     */
    private $slug;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="children")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * })
     */
    private $parent;


    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(name="level", type="integer", nullable=true)
     */
    private $level;

    /**
     * @ORM\OneToMany(targetEntity="Category" , mappedBy="parent")
     * @ORM\OrderBy({"name" = "ASC"})
     **/
    private $children;

    /**
     * @Gedmo\TreePath(separator="/")
     * @ORM\Column(name="path", type="string", length=300, nullable=true)
     */
    private $path;

    /**
     * @var int
     *
     * @ORM\Column(name="sorting", type="smallint")
     */
    private $sorting = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="permission", type="string")
     */
    private $permission = 'public';

    /**
     * @var boolean
     *
     * @ORM\Column(name="isDelete", type="boolean")
     */
    private $isDelete = false;


    /**
     * @var integer
     *
     * @ORM\Column(name="code", type="integer", nullable = true)
     */
    private $code;


    /**
     * @var integer
     *
     * @ORM\Column(name="sku", type="string", length=3, nullable = true)
     */
    private $sku;


    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status = true;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }



    /**
     * Get name
     *
     * @return string
     */
    public function getNamewithMode()
    {
        return $this->itemMode." - ".$this->name;
    }



    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Category
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set code
     *
     * @param integer $code
     *
     * @return Category
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return integer
     */
    public function getCode()
    {
        return $this->code;
    }


    /**
     * Set status1
     *
     * @param boolean $status1
     *
     * @return Category
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function isStatus()
    {
        return $this->status;
    }


    /**
     * @return mixed
     */
    public function getSTRPadCode()
    {
        $code = str_pad($this->getCode(),2, '0', STR_PAD_LEFT);
        return $code;
    }


    /**
     * @return Category
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }
    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @return boolean
     */
    public function isFeature()
    {
        return $this->feature;
    }

    /**
     * @param boolean $feature
     */
    public function setFeature($feature)
    {
        $this->feature = $feature;
    }

    /**
     * @return int
     */
    public function getPermission()
    {
        return $this->permission;
    }

    /**
     * @param int $permission
     */
    public function setPermission($permission)
    {
        $this->permission = $permission;
    }

    /**
     * @return int
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * @param int $sorting
     */
    public function setSorting($sorting)
    {
        $this->sorting = $sorting;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }


    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    public function getNestedLabel()
    {
        if($this->getLevel() > 1) {
            return $this->formatLabel($this->getLevel() - 1, $this->getName());
        }else{
            return $this->getName();
        }
    }

    public function getParentIdByLevel($level = 1)
    {
        $parentsIds = explode("/", $this->getPath());

        return isset($parentsIds[$level - 1]) ? $parentsIds[$level - 1] : null;

    }

    private function formatLabel($level, $value) {
        return str_repeat("-", $level * 3) . str_repeat(">", $level) . $value;
    }


    /**
     * @return CategoryMeta
     */
    public function getCategoryMetas()
    {
        return $this->categoryMetas;
    }

    /**
     * @return int
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param int $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }



    /**
     * @return StockItem
     */
    public function getStockItems()
    {
        return $this->stockItems;
    }

    /**
     * @return GenericMaster
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param GenericMaster $config
     */
    public function setConfig($config): void
    {
        $this->config = $config;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size): void
    {
        $this->size = $size;
    }

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param mixed $color
     */
    public function setColor($color): void
    {
        $this->color = $color;
    }

    /**
     * @return mixed
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param mixed $brand
     */
    public function setBrand($brand): void
    {
        $this->brand = $brand;
    }

    /**
     * @return Head
     */
    public function getGeneralLedger()
    {
        return $this->generalLedger;
    }

    /**
     * @param Head $generalLedger
     */
    public function setGeneralLedger($generalLedger)
    {
        $this->generalLedger = $generalLedger;
    }

    /**
     * @return string
     */
    public function getGlCode()
    {
        return $this->glCode;
    }

    /**
     * @param string $glCode
     */
    public function setGlCode($glCode)
    {
        $this->glCode = $glCode;
    }

    /**
     * @return StockBook
     */
    public function getStockBooks()
    {
        return $this->stockBooks;
    }

    /**
     * @return Item
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return string
     */
    public function getItemMode()
    {
        return $this->itemMode;
    }

    /**
     * @param string $itemMode
     */
    public function setItemMode($itemMode)
    {
        $this->itemMode = $itemMode;
    }

    /**
     * @return bool
     */
    public function isDelete()
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     */
    public function setIsDelete($isDelete)
    {
        $this->isDelete = $isDelete;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy): void
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created): void
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated): void
    {
        $this->updated = $updated;
    }

}


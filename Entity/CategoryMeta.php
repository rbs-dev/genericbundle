<?php

namespace Terminalbd\GenericBundle\Entity;

use App\Entity\Application\GenericMaster;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * PageMeta
 *
 * @ORM\Table(name="gmb_categorymeta")
 * @ORM\Entity(repositoryClass="Terminalbd\GenericBundle\Repository\CategoryMetaRepository")
 */
class CategoryMeta
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var GenericMaster
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\GenericMaster")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private $config;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\Category", inversedBy="categoryMetas" )
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $category;


    /**
     * @var string
     *
     * @ORM\Column(name="metaLang", type="string", nullable= true)
     */
    private $metaLang;

    /**
     * @var string
     *
     * @ORM\Column(name="metaKey", type="string", nullable= true)
     */
    private $metaKey;

    /**
     * @var string
     *
     * @ORM\Column(name="metaValue", type="string", nullable= true)
     */
    private $metaValue;

    /**
     * @var string
     *
     * @ORM\Column(type="string" ,nullable = true)
     */
    private $inputType;




    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Page
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param Page $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }


    /**
     * @param int $showLimit
     */
    public function setShowLimit($showLimit)
    {
        $this->showLimit = $showLimit;
    }

    /**
     * @return string
     */
    public function getMetaKey()
    {
        return $this->metaKey;
    }

    /**
     * @param string $metaKey
     */
    public function setMetaKey($metaKey)
    {
        $this->metaKey = $metaKey;
    }

    /**
     * @return string
     */
    public function getMetaValue()
    {
        return $this->metaValue;
    }

    /**
     * @param string $metaValue
     */
    public function setMetaValue($metaValue)
    {
        $this->metaValue = $metaValue;
    }

    /**
     * @return string
     */
    public function getMetaLang()
    {
        return $this->metaLang;
    }

    /**
     * @param string $metaLang
     */
    public function setMetaLang($metaLang)
    {
        $this->metaLang = $metaLang;
    }

    /**
     * @return AssetsCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param AssetsCategory $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return GenericMaster
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param GenericMaster $config
     */
    public function setConfig()
    {
        $this->config = $config;
    }

    /**
     * @return string
     */
    public function getInputType()
    {
        return $this->inputType;
    }

    /**
     * @param string $inputType
     */
    public function setInputType($inputType)
    {
        $this->inputType = $inputType;
    }





}

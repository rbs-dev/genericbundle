<?php

namespace Terminalbd\GenericBundle\Entity;
use App\Entity\Application\GenericMaster;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\GenericBundle\Repository\ItemBrandRepository")
 * @ORM\Table(name="gmb_item_brand")
 * @UniqueEntity(fields={"name","config"}, message="This name must be unique")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ItemBrand
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var GenericMaster
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\GenericMaster")
     */
    private $config;


    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $name;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @Doctrine\ORM\Mapping\Column(length=255,unique=false)
     */
    private $slug;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer",nullable=true)
     */
    private $code;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDelete = false;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }



    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }


    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return GenericMaster
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param GenericMaster $config
     */
    public function setConfig($config): void
    {
        $this->config = $config;
    }

    /**
     * @return bool
     */
    public function isDelete()
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     */
    public function setIsDelete($isDelete)
    {
        $this->isDelete = $isDelete;
    }






}

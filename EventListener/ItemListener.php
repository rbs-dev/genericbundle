<?php

namespace Terminalbd\GenericBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Terminalbd\GenericBundle\Entity\Item;

class ItemListener
{
    public function prePersist(LifecycleEventArgs $args)
    {
        $this->createCode($args);
    }

    public function createCode(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        // perhaps you only want to act on some "Sales" entity
        if ($entity instanceof Item) {

            $lastCode = $this->getLastCode($args,$entity);
           // $productGroup = $entity->getProductGroup()->getCode();
            $entity->setCode($lastCode+1);
           // $entity->setItemCode(sprintf("%s%s", $productGroup, str_pad($entity->getCode(),4, '0', STR_PAD_LEFT)));
            $entity->setItemCode(sprintf("%s",str_pad($entity->getCode(),4, '0', STR_PAD_LEFT)));
        }
    }

    /**
     * @param LifecycleEventArgs $args
     * @param $datetime
     * @param $entity
     * @return int|mixed
     */
    public function getLastCode(LifecycleEventArgs $args,$entity)
    {

        $entityManager = $args->getEntityManager();
        $qb = $entityManager->getRepository(Item::class)->createQueryBuilder('s');
        $qb
            ->select('MAX(s.code)')
            ->where('s.config = :config')
            ->setParameter('config', $entity->getConfig());
        $lastCode = $qb->getQuery()->getSingleScalarResult();
        if (empty($lastCode)) {
            return 0;
        }
        return $lastCode;
    }
}
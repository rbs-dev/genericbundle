<?php
namespace Terminalbd\GenericBundle\Form;

use App\Entity\Application\GenericMaster;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Entity\ItemBrand;
use Terminalbd\GenericBundle\Entity\ItemColor;
use Terminalbd\GenericBundle\Entity\ItemSize;
use Terminalbd\GenericBundle\Entity\ItemUnit;
use Terminalbd\GenericBundle\Entity\MasterItem;
use Terminalbd\GenericBundle\Entity\Particular;


/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ItemFormType extends AbstractType
{



    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $config =  $options['config']->getId();

        $builder

            ->add('name', TextType::class, [
                'attr' => ['autofocus' => true],
                'label' => 'label.name',
            ])


            /*
              ->add('category', EntityType::class, [
                'class' => Category::class,
                'attr'=>['class'=>'select2'],
                'required' => false,
                'placeholder' => 'Choose a name',
                'choice_label' => 'nestedLabel',
                'choices'   => $options['categoryRepo']->getFlatCategoryTree($options['config'])
            ])
             ->add('productGroup', EntityType::class, [
                'class' => Particular::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) use($config){
                    return $er->createQueryBuilder('e')
                        ->join("e.particularType","st")
                        ->where("st.slug ='product-group'")
                        ->andWhere("e.status =1")
                        ->andWhere("e.config ={$config}")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'col-md-6 select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a  product group',
            ])*/
            ->add('unit', EntityType::class, array(
                'required'    => true,
                'class' => ItemUnit::class,
                'placeholder' => 'Choose a  product unit',
                'choice_label' => 'name',
                'attr'=>array('class'=>'select2'),
                'query_builder' => function(EntityRepository $er) use($config){
                    return $er->createQueryBuilder('e')
                        ->where("e.config ={$config}")
                        ->orderBy('e.status', 'ASC');
                },
            ))
            ->add('purchasePrice',TextType::class,
                array(
                    'attr'=>array('class'=>'m-wrap purchasePrice number','placeholder'=>'Purchase price'),
                    'required'    => false,
                )
            )
            ->add('salesPrice',TextType::class,
                array(
                    'attr'=>array('class'=>'m-wrap productionPrice number','placeholder'=>'Sales price'),
                    'required'    => false,
                )
            )
            ->add('itemCode',TextType::class,
                array(
                    'attr'=>array('class'=>'','placeholder'=>'Item Code'),
                    'required'    => false,
                )
            )
            ->add('minQuantity',TextType::class,
                array(
                    'attr'=>array('class'=>'m-wrap span12 number','placeholder'=>'Enter min quantity'),
                    'required'    => false,
                )
            )
             ->add('reorderQuantity',TextType::class,
                array(
                    'attr'=>array('class'=>'m-wrap span12 number','placeholder'=>'Enter reorder quantity'),
                    'required'    => false,
                )
            )->add('Save_with_Continue', SubmitType::class, [
                'attr' => ['class' => 'btn btn-info form-submit btn btn-hidden'],
            ])
           ;

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Item::class,
            'config' => GenericMaster::class,
            'categoryRepo' => CategoryRepository::class,
        ]);
    }
}

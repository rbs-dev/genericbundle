<?php

namespace Terminalbd\GenericBundle\Repository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\GenericBundle\Entity\CategoryMeta;
use Terminalbd\GenericBundle\Entity\ItemKeyValue;
use Terminalbd\GenericBundle\Entity\Particular;


/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class CategoryMetaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CategoryMeta::class);
    }

    public function categoryMeta(Category $category, $data)
    {

        $em = $this->_em;
        if ($category and isset($data['metaValue']) and !empty($data['metaValue'])) {
            $i = 0;
            if(isset($data['metaKey']) OR isset($data['metaValue']) ){
                foreach ($data['metaKey'] as $value) {
                    $metaId = isset($data['metaId'][$i]) ? $data['metaId'][$i] : '' ;
                    if(!empty($metaId)){
                        $itemKeyValue = $this->findOneBy(array('category' => $category,'id' => $metaId));
                        if($itemKeyValue){
                            $this->updateMetaAttribute($itemKeyValue,$data['metaKey'][$i],$data['metaValue'][$i],$data['inputType'][$i]);
                        }
                    }elseif(($data['metaKey'][$i] != "" and empty($metaId))){
                        $entity = new CategoryMeta();
                    //    $entity->setConfig($category->getConfig());
                        $entity->setMetaKey($data['metaKey'][$i]);
                        $entity->setMetaValue($data['metaValue'][$i]);
                        $entity->setInputType($data['inputType'][$i]);
                        $entity->setCategory($category);
                        $em->persist($entity);
                        $em->flush($entity);
                    }
                    $i++;
                }
            }

            $em->flush();

        }

    }

    public function updateMetaAttribute(CategoryMeta $itemKeyValue , $key , $value ='', $inputType ='')
    {
        $em = $this->_em;
        $itemKeyValue->setMetaKey($key);
        $itemKeyValue->setMetaValue($value);
        $itemKeyValue->setMetaValue($value);
        $itemKeyValue->setInputType($inputType);
        $em->flush();
    }



}

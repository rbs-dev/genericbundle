<?php

namespace Terminalbd\GenericBundle\Repository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Terminalbd\GenericBundle\Entity\ItemKeyValue;
use Terminalbd\GenericBundle\Entity\Particular;


/**
     * This custom Doctrine repository contains some methods which are useful when
     * querying for blog post information.
     *
     * See https://symfony.com/doc/current/doctrine/repository.html
     *
     * @author Md Shafiqul islam <shafiqabs@gmail.com>
     */
class ItemKeyValueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ItemKeyValue::class);
    }


    public function insertSettingKeyValue(Particular $reEntity,$data)
    {
        $em = $this->_em;
        $i=0;
        if(isset($data['metaKey']) OR isset($data['metaValue']) ){
            foreach ($data['metaKey'] as $value) {
                $metaId = isset($data['metaId'][$i]) ? $data['metaId'][$i] : '' ;
                if(!empty($metaId)){
                    $itemKeyValue = $this->findOneBy(array('particular' => $reEntity,'id' => $metaId));
                    if($itemKeyValue){
                        $this->updateMetaAttribute($itemKeyValue,$data['metaKey'][$i],$data['metaValue'][$i]);
                    }
                }elseif(($data['metaKey'][$i] != "" and empty($metaId))){
                    $entity = new ItemKeyValue();
                    $entity->setMetaKey($data['metaKey'][$i]);
                    $entity->setMetaValue($data['metaValue'][$i]);
                    $entity->setSetting($reEntity);
                    $em->persist($entity);
                    $em->flush($entity);
                }
                $i++;
            }
        }
    }

    public function updateMetaAttribute(ItemKeyValue $itemKeyValue , $key , $value ='')
    {
        $em = $this->_em;
        $itemKeyValue->setMetaKey($key);
        $itemKeyValue->setMetaValue($value);
        $em->flush();
    }

    public function setDivOrdering($data)
    {
        $i = 1;
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        foreach ($data as $key => $value){
            $qb->update('TerminalbdGenericBundle:ItemKeyValue', 'mg')
                ->set('mg.sorting', $i)
                ->where('mg.id = :id')
                ->setParameter('id', $key)
                ->getQuery()
                ->execute();
            $i++;

        }

    }

}
